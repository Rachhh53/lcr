// text_read.cpp  
// compile with: /clr 
// This outputs the rules of LCR from a text file to the console.

#include "stdafx.h"
#include "Rules.h"
#using<system.dll>  
using namespace System;
using namespace System::IO;

Rules::Rules()
{
	// create variable for the file to be read
	String^ fileName = "Rules.txt";
	try
	{
		//Console::WriteLine("trying to open file {0}...", fileName);
		StreamReader^ din = File::OpenText(fileName);

		// store each line as string and output it
		String^ str;
		int count = 0;
		while ((str = din->ReadLine()) != nullptr)
		{
			count++;
			Console::WriteLine(str);
		}
	}
	// If there is no file stored in location or if the file is unreadable,
	// output an error to the console.
	catch (Exception^ e)
	{
		if (dynamic_cast<FileNotFoundException^>(e))
			Console::WriteLine("file '{0}' not found", fileName);
		else
			Console::WriteLine("problem reading file '{0}'", fileName);
	}

	return;
}
