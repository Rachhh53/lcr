#include "stdafx.h"
#include "RollDice.h"

unsigned int RollDice::Roll()
{
	unsigned int diceVal = 0;
	diceVal = EachRoll();

	return diceVal;
}

	//Roll dice by generating random number between 1 and 6
	unsigned int RollDice::EachRoll() {
		int max = 6;
		int diceRoll = 0;
		unsigned int diceVal = 0;

		// Uses computer internal clock to seed
		srand(time(NULL));
		// We add 1 because this function will output between 0 and max - 1
		diceRoll = rand() % max + 1;

		getDice(diceRoll);

		return diceRoll;

	}

	// Display dice to user
	void RollDice::getDice(int diceVal) {

		switch (diceVal) {

		case 1:
			cout << "#########" << endl;
			cout << "#       #" << endl;
			cout << "#   0   #" << endl;
			cout << "#       #" << endl;
			cout << "#########" << endl;
			
			return;

		case 2:
			cout << "#########" << endl;
			cout << "# 0     #" << endl;
			cout << "#       #" << endl;
			cout << "#     0 #" << endl;
			cout << "#########" << endl;

			return;

		case 3:
			cout << "#########" << endl;
			cout << "# 0     #" << endl;
			cout << "#   0   #" << endl;
			cout << "#     0 #" << endl;
			cout << "#########" << endl;

			return;

		case 4:
			cout << "#########" << endl;
			cout << "# 0   0 #" << endl;
			cout << "#       #" << endl;
			cout << "# 0   0 #" << endl;
			cout << "#########" << endl;

			return;

		case 5:
			cout << "#########" << endl;
			cout << "# 0   0 #" << endl;
			cout << "#   0   #" << endl;
			cout << "# 0   0 #" << endl;
			cout << "#########" << endl;

			return;

		case 6:
			cout << "#########" << endl;
			cout << "# 0   0 #" << endl;
			cout << "# 0   0 #" << endl;
			cout << "# 0   0 #" << endl;
			cout << "#########" << endl;

			return;
		}


	}
