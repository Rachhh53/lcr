#pragma once

#include <string>
#include <iostream>

using namespace std;

class Player
{
private:
	// Number of players cannot be negative
	unsigned int NumberPlayers;


public:
	Player() {
	}

	void SetNumPlayers(const unsigned int& numPlayers) {
		/*	Players.resize(numPlayers + 1);*/
		NumberPlayers = numPlayers;
	}

	const int GetNumPlayers() const {
		return NumberPlayers;
	}

};
