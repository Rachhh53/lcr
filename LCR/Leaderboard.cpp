#include "stdafx.h"
#include "Game.h"
#include "Leaderboard.h"

#include <mysql.h>
#include <iostream>
#include <string>

using namespace std;
//int qstate;

void Game::DisplayLeaderboard()
{
	// clear the vector for subsequent calls so we don't add structs twice
	Standings.clear();
	MYSQL* conn;
	MYSQL_ROW row;
	MYSQL_RES* res;
	conn = mysql_init(0);

	// server, user, password, database, port
	conn = mysql_real_connect(conn, "localhost", "root", "password", "lcr", 3306, NULL, 0);

	if (conn) {
		// FOR TESTING DB CONNECTION
		//puts("Successful connection to database!");

		// get 5 lowest times
		string query = "SELECT * FROM scores ORDER BY time ASC LIMIT 5";
		const char* q = query.c_str();
		qstate = mysql_query(conn, q);
		if (!qstate)
		{
			res = mysql_store_result(conn);
			while (row = mysql_fetch_row(res))
			{
                int NameLength = 0;
				int TimeLength = 0;
                const int NameSpaces = 52; // between name and time
				const int TimeSpaces = 12; // total space time should take up
				int NameSpacesCalc = 0;
				int TimeSpacesCalc = 0;

				PlayerStandings player;
				// row[0] = id, row[1] = name, row [2] = time
				player.PlayerName = string(row[1]);
				player.PlayerTime = string(row[2]);

				NameLength = player.PlayerName.length();
				TimeLength = player.PlayerTime.length();
				
				NameSpacesCalc = (NameSpaces - NameLength);
				TimeSpacesCalc = (TimeSpaces - TimeLength);

				if (NameSpacesCalc > 0) {
					for (unsigned int i = 0; i <= NameSpacesCalc; ++i) {
						player.PlayerSpaces = player.PlayerSpaces + " ";
					}
				}

				if (TimeSpacesCalc > 0) {
					for (unsigned int i = 0; i <= TimeSpacesCalc; ++i) {
						player.PlayerTime = player.PlayerTime + " ";
					}
				}
				// add 'em to the vector!
				Standings.push_back(player);
			}
		}
		else
		{
			cout << "Query failed: " << mysql_error(conn) << endl;
		}
	}
	else {
		puts("Connection to database has failed!");
	}
	// for testing db
	//printf("ID: %s, Name: %s, Value: %s\n", row[0], row[1], row[2]);

	// print leaderboard with players and times and correct spacing
	cout << "*******************************************************************************************" << endl;
	cout << "*                               Player Leaderboard                                        *" << endl;
	cout << "*******************************************************************************************" << endl;
	cout << "*          Player Name                                    Player Time                     *" << endl;
	if (Standings.size() > 0) {
		cout << "* 1. " << Standings.at(0).PlayerName << Standings.at(0).PlayerSpaces << Standings.at(0).PlayerTime << "                   " << "*" << endl;
	}
	if (Standings.size() > 1) {
		cout << "* 2. " << Standings.at(1).PlayerName << Standings.at(1).PlayerSpaces << Standings.at(1).PlayerTime << "                   " << "*" << endl;
	}
	if (Standings.size() > 2) {
		cout << "* 3. " << Standings.at(2).PlayerName << Standings.at(2).PlayerSpaces << Standings.at(2).PlayerTime << "                   " << "*" << endl;
	}
	if (Standings.size() > 3) {
		cout << "* 4. " << Standings.at(3).PlayerName << Standings.at(3).PlayerSpaces << Standings.at(3).PlayerTime << "                   " << "*" << endl;
	}
	if (Standings.size() > 4) {
		cout << "* 5. " << Standings.at(4).PlayerName << Standings.at(4).PlayerSpaces << Standings.at(4).PlayerTime << "                   " << "*" << endl;
	}
	cout << "*******************************************************************************************" << endl;

	return;
}


	// for testing until we store the values
	//cout << "*******************************************************************************************" << endl;
	//cout << "*                               Player Leaderboard                                        *" << endl;
	//cout << "*******************************************************************************************" << endl;
	//cout << "*          Player Name                                    Player Time                     *" << endl; //36 spaces
	//cout << "* 1. Player1                 " << "                              Time 1                         " << "*" << endl;
	//cout << "* 2. Player2                 " << "                              Time 2                         " << "*" << endl;
	//cout << "* 3. Player3                 " << "                              Time 3                         " << "*" << endl;
	//cout << "* 4. Player4                 " << "                              Time 4                         " << "*" << endl;
	//cout << "* 5. Player5                 " << "                              Time 5                         " << "*" << endl;
	//cout << "*******************************************************************************************" << endl;
	//return;
//
//};

void Game::InsertScore(int FinalTime, string playerName)
{
	string time = to_string(FinalTime);
	MYSQL* conn;
	MYSQL_ROW row;
	MYSQL_RES* res;
	conn = mysql_init(0);

	// server, user, password, database, port
	conn = mysql_real_connect(conn, "localhost", "root", "password", "lcr", 3306, NULL, 0);

	if (conn) {
		// FOR TESTING DB CONNECTION
		//puts("Successful connection to database!");

		string query = "INSERT INTO scores (name, time) VALUES (\"" +playerName+ "\", " +time+ ")";
		const char* q = query.c_str();
		// FOR TESTING INSERT
		//cout << q << endl;
		qstate = mysql_query(conn, q);
		if (!qstate)
		{
			res = mysql_store_result(conn);
			//while (row = mysql_fetch_row(res))
			//{
			// FOR TESTING
				//cout << "Player added to Leaderboard" << endl;
			//}
		}
		else
		{
			cout << "Query failed: " << mysql_error(conn) << endl;
		}
	}
	else {
		puts("Connection to database has failed!");
	}
}