#pragma once
#include "stdafx.h"
//#include "Game.h"
#include <iostream>
#include <ctime>
#include <random>

using namespace std;

class RollDice
{
private:
	// Function declarations
	unsigned int EachRoll();
	void getDice(int);

	// Dice
	unsigned int dieOne = 0;
	unsigned int dieTwo = 0;
	unsigned int dieThree = 0;

public:
	unsigned int Roll();
};

